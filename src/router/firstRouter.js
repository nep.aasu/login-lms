import { Router } from "express";

let firstRouter = Router();

firstRouter
  .route("/") //localhost:8000  while selecting api it sees the route it does not sees the url

  .post((req, res) => {
    console.log(`body send data: `, req.body);
    console.log(`query send data: `, req.query);
    console.log(`params send data: `, req.params);
    res.json("home post");
    // one response to one request
  });

firstRouter.route("/name").post((req, res) => {
  res.json("name post");
});

firstRouter.route("/a/:country/b/:name").post((req, res) => {
  console.log("params", req.params);
  res.json(":a response hello");
});
export default firstRouter;
