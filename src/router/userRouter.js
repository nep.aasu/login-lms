import { Router } from "express";
import Joi from "joi";
import {
  createUser,
  deleteSpecificUser,
  forgetPassword,
  myProfile,
  readAllUser,
  readSpecificUser,
  resetPassword,
  updatePassword,
  updateProfile,
  updateSpecificUser,
  userLogin,
  verifyEmail,
} from "../controller/userController.js";
import authorized from "../middleware/authorized.js";
import { isAuthenticated } from "../middleware/isAuthenticated.js";

import { validation } from "../middleware/validation.js";
import { userValidation } from "../validation/userValidation.js";

let userRouter = Router();

/* 
 "fullName":"aashutosh",
    "email":"nep.aas@gmail.com",
    "password":"Password@123",
    "gender":"male",
    "dob":"1999-01-01",
    "role":"admin"
}
*/


userRouter
  .route("/")
  .post(validation(userValidation), createUser)
  .get(isAuthenticated, authorized(["admin", "superAdmin"]), readAllUser);

userRouter.route("/verify-email").post(verifyEmail);

userRouter.route("/login").post(userLogin);

userRouter.route("/my-profile").get(isAuthenticated, myProfile);

userRouter.route("/update-profile").patch(isAuthenticated, updateProfile);

userRouter.route("/update-password").patch(isAuthenticated, updatePassword);

userRouter.route("/forget-password").post(forgetPassword);

userRouter.route("/reset-password").patch(isAuthenticated, resetPassword);

userRouter
  .route("/:id")
  .get(isAuthenticated, authorized(["admin", "superAdmin"]), readSpecificUser)
  .patch(
    isAuthenticated,
    authorized(["admin", "superAdmin"]),
    updateSpecificUser
  )
  .delete(isAuthenticated, authorized(["superAdmin"]), deleteSpecificUser);

export default userRouter;
