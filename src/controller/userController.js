import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";

import { secretKey } from "../config.js";
import { User } from "../schema/model.js";
import { sendMail } from "../utils/sendMail.js";

export let createUser = async (req, res, next) => {
  let data = req.body;
  // save data to User
  try {
    let password = data.password;
    let hashPassword = await bcrypt.hash(password, 10);

    data = {
      ...data,
      password: hashPassword,
    };
    let result = await User(data);
    // send email
    // generate token
    let infoObj = {
      data: result,
    };
    let expiryInfo = {
      expiresIn: "365d",
    };

    let token = await jwt.sign(infoObj, secretKey, expiryInfo);
    // link -> frontend link
    //  send mail
    await sendMail({
      from: ' "register Web User" "<noreply>"',
      to: [data.email],
      //   attachments: [
      //     {
      //       filename: "1702799211365paisa.mp4",
      //       path: "/Users/aashutoshkayastha/Documents/DW9/EXPRESS/public/1702799211365paisa.mp4",
      //     },
      //   ],
      subject: "Verify Your Email",

      html: `<h1>Click the link below to earn money</h1>
        <h1>Congratulations!</h1>
        
        <img src="https://media1.tenor.com/m/KNvKExU2dAsAAAAd/paisa-hi-paisa-hoga-hera-pheri.gif"  width="500" height="600">
        <br></br>
        <a href = "http://localhost:3000/verify-email?token=${token}">
        http://localhost:3000/verify-email?token=${token}
        </a>
  
        `,
    });

    res.status(201).json({
      success: true,
      message: "user created successfully.",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};

export let readAllUser = async (req, res, next) => {
  try {
    let result = await User.find({});
    res.status(200).json({
      success: true,
      message: "readAllUser read successfully",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};

export let verifyEmail = async (req, res, next) => {
  try {
    let token = req.headers.authorization.split(" ")[1];

    let infoObj = jwt.verify(token, secretKey);

    let data = infoObj.data;
    console.log(data);
    let result = await User.create(data);
    res.status(201).json({
      success: true,
      message: "email verified successfully",
      data: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};

export let userLogin = async (req, res, next) => {
  try {
    let email = req.body.email;
    let password = req.body.password;
    let user = await User.findOne({ email: email });
    console.log(email);
    if (user) {
      let isValidPassword = await bcrypt.compare(password, user.password);

      if (isValidPassword) {
        let infoObj = {
          _id: user._id,
        };
        let expiryInfo = {
          expiresIn: "365d",
        };
        let token = await jwt.sign(infoObj, secretKey, expiryInfo);

        res.status(200).json({
          success: true,
          message: "User Login successful",
          data: user,
          token: token,
        });
      } else {
        let error = new Error("Credential does not match.");
        throw error;
      }
    } else {
      let error = new Error("Credential does not match.");
      throw error;
    }
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};

export let myProfile = async (req, res, next) => {
  let _id = req._id;

  try {
    let result = await User.findById(_id);

    res.status(200).json({
      success: true,
      message: "my-profile read successfully",
      data: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};

export let updateProfile = async (req, res, next) => {
  try {
    let _id = req._id;
    let data = req.body;
    delete data.email;
    delete data.password;
    let result = await User.findByIdAndUpdate(_id, data, { new: true });
    res.status(201).json({
      success: true,
      message: "Profile Updated successfully",
      data: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};

export let updatePassword = async (req, res, next) => {
  try {
    let _id = req._id;
    let oldPassword = req.body.oldPassword;
    let newPassword = req.body.newPassword;
    console.log(newPassword);
    let data = await User.findById(_id);
    let hashPassword = data.password;
    let isValidPassword = await bcrypt.compare(oldPassword, hashPassword);

    if (isValidPassword) {
      if (oldPassword.toLowerCase() !== newPassword.toLowerCase()) {
        let newHashPassword = await bcrypt.hash(newPassword, 10);
        let result = await User.findByIdAndUpdate(
          _id,
          { password: newHashPassword },
          {
            new: true,
          }
        );
        res.json({
          success: true,
          message: "update password successfully",
          data: result,
        });
      } else {
        let error = new Error("newPassword and old password cannot be same");
        throw error;
      }
    } else {
      let error = new Error("Old Password does not match");
      throw error;
    }
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};

export let readSpecificUser = async (req, res, next) => {
  try {
    let _id = req.params.id;
    let result = await User.findById(_id);
    res.status(200).json({
      success: true,
      message: "readSpecificUser read successfully",
      data: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};

export let updateSpecificUser = async (req, res, next) => {
  try {
    let _id = req.params.id;
    let data = req.body;
    delete data.email;
    delete data.password;

    let result = await User.findByIdAndUpdate(_id, data, { new: true });
    res.status(201).json({
      success: true,
      message: "readSpecificUser read successfully",
      data: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};

export let deleteSpecificUser = async (req, res) => {
  try {
    let _id = req.params.id;

    let result = await User.findByIdAndDelete(_id);
    res.status(200).json({
      success: true,
      message: "deleteSpecificUser read successfully",
      data: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};

export let forgetPassword = async (req, res, next) => {
  try {
    let email = req.body.email;

    let result = await User.findOne({ email: email });
    if (result) {
      let infoObj = {
        _id: result._id,
      };
      let expiryInfo = {
        expiresIn: "365d",
      };
      let token = await jwt.sign(infoObj, secretKey, expiryInfo);

      await sendMail({
        from: '"<noreply>"',
        to: email,
        subject: "Reset password",
        html: `<h1>Please click the link to reset your password </h1>
          <br></br>
          <a href = "http://localhost:3000/reset-password?token=${token}">
          http://localhost:3000/reset-password?token=${token}
          </a>
    
          `,
      });

      res.status(200).json({
        success: true,
        message: "reset password link has been send to your email",
      });
    } else {
      res.status(404).json({
        success: false,
        message: "email not found",
      });
    }
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};

export let resetPassword = async (req, res, next) => {
  try {
    let hashPassword = await bcrypt.hash(req.body.password, 10);

    let result = await User.findByIdAndUpdate(
      req._id,
      {
        password: hashPassword,
      },
      {
        new: true,
      }
    );
    res.status(201).json({
      success: true,
      message: "resetPassword successfully",
      data: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};

/* 
find({})
findById(id)
findByIdAndUpdate(id,data,{new:true})
findByIdAndDelete(id)

STATUS_CODE

success
2xx
create: 201
read:200
update:201
delete:200


error
4xx
400
401: token not valid, credential not match 403
403: token is valid but not authorized
409: conflict duplicate key
404: api not found
*/
