import mongoose from "mongoose";
import { dbUrl } from "../config.js";

const connectToMongoDB = async () => {
  try {
    mongoose.connect(`${dbUrl}`);
    console.log(`connected at port ${dbUrl}`);
  } catch (error) {
    console.log(error.message);
  }
};
export default connectToMongoDB;
