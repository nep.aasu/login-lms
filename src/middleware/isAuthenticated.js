import jwt from "jsonwebtoken";
import { secretKey } from "../config.js";

export let isAuthenticated = async (req, res, next) => {
  let bearerToken = req.headers.authorization;
  let token = bearerToken.split(" ")[1];
  try {
    let user = await jwt.verify(token, secretKey);
    let _id = user._id;
    req._id = _id;
    next();
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};
