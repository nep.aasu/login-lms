import jwt from "jsonwebtoken";
import { secretKey } from "../config.js";

export const isVerifiedEmail = (req, res, next) => {
  try {
    let token = req.headers.authorization.split(" ")[1];
    let infoObj = jwt.verify(token, secretKey);
    req.infoObj = infoObj;
    next();
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};
