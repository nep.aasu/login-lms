import cors from "cors";
import { config } from "dotenv";
import express, { json } from "express";
import { port } from "./src/config.js";

import connectToMongoDB from "./src/database/mongodbConnect.js";

import firstRouter from "./src/router/firstRouter.js";
import userRouter from "./src/router/userRouter.js";

// router import end

config();
const expressApp = express();
expressApp.use(json()); // always place this code at top of the router
expressApp.use(express.static("./public"));
expressApp.use(cors());

expressApp.listen(port, () => {
  console.log(`express app is listening at port ${port}`);
});

connectToMongoDB();
expressApp.use("/", firstRouter);
expressApp.use("/users", userRouter);
